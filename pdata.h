#ifndef _PDATA_H_
#define _PDATA_H_
typedef struct PDataDef{
    char*name;
    int count;//the count of dat.
    struct PDataDef*dat;
    char*text;
}PData;
/*pderror:return the error message*/
char*pderror();
/*getPData:read the PData file and assign it to the pdata.
if successful,it returns 1,if not,it returns 0*/
int getPData(char*file,PData*pdata);
/*putPData:output the pdata to the file.
if successful,it returns 1,if not,it returns 0*/
int putPData(char*file,PData pdata);
/*addPData:add the child to pdata.dat*/
void addPData(PData*pdata,PData child);
/*get the index of dat with the name.
If no found,it will return -1*/
int searchPData(PData pdata,char*name);
#endif