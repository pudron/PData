#include"pdata.h"
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
static int ptr,line;
static char*code,*fileName;
static char*msg;
char*pderror(){
    return msg;
}
static void mistake(char*text){
    msg=(char*)malloc(strlen(text)+64);
    sprintf(msg,"%s:%d:error:%s\n",fileName,line,text);
}
static int readFile(char*name){
    FILE*fp=fopen(name,"rt");
    if(fp==NULL){
        mistake("failed to open the file.");
        return 0;
    }
    fseek(fp,0,SEEK_END);
    int len=ftell(fp);
    rewind(fp);
    code=(char*)malloc(len+1);
    len=fread(code,1,len,fp);
    code[len]='\0';
    fclose(fp);
    return 1;
}
static void skip(){
    char c=code[ptr];
    while(c==' ' || c=='\n'){
        if(c=='\n'){
            line++;
        }
        c=code[++ptr];
    }
}
static void skipLine(){
    char c=code[ptr];
    while(c!='\n'){
        if(c=='\0'){
            return;
        }
        c=code[++ptr];
    }
    ptr++;
    line++;
}
static int getHead(PData*pdata){
    char temp[1024];
    char c=code[++ptr];
    int i;
    for(i=0;c!=')';i++){
        if(c=='\0'){
            mistake("expected a\")\".");
            return 0;
        }else if(c=='\n'){
            line++;
        }else{
            temp[i]=c;
            c=code[++ptr];
        }
    }
    ptr++;
    temp[i]='\0';
    pdata->name=(char*)malloc(i+1);
    strcpy(pdata->name,temp);
    return 1;
}
static int getString(PData*pdata){
    char temp[1024];
    char c=code[++ptr];
    int i;
    for(i=0;c!='\"';i++){
        if(c=='\0'){
            mistake("expected a \".");
            return 0;
        }else if(c=='\n'){
            line++;
        }else if(c=='\\'){
            switch(code[++ptr]){
                case 'n':
                    temp[i]='\n';
                    break;
                case '\\':
                    temp[i]='\\';
                    break;
                case '0':
                    temp[i]='\0';
                    break;
                /*case '\'':
                    temp[i]='\'';
                    break;*/
                case '\"':
                    temp[i]='\"';
                    break;
            }
            c=code[++ptr];
        }else{
            temp[i]=c;
            c=code[++ptr];
        }
    }
    temp[i]='\0';
    ptr++;
    pdata->text=(char*)realloc(pdata->text,i+1+strlen(pdata->text));
    strcat(pdata->text,temp);
    return 1;
}
static int getDat(PData*pdata){
    skip();
    pdata->dat=NULL;
    pdata->text=(char*)malloc(1);
    pdata->text[0]='\0';
    pdata->count=0;
    if(!getHead(pdata)){
        return 0;
    }
    skip();
    if(code[ptr]!='{'){
        mistake("expected a \"{\".");
        return 0;
    }
    char c;
    ptr++;
    while(1){
        skip();
        c=code[ptr];
        if(c=='#'){
            skipLine();
        }else if(c=='\"'){
            if(!getString(pdata)){
                return 0;
            }
        }else if(c=='('){
            PData sub;
            getDat(&sub);
            pdata->count++;
            pdata->dat=(PData*)realloc(pdata->dat,pdata->count*sizeof(PData));
            pdata->dat[pdata->count-1]=sub;
        }else if(c=='}'){
            ptr++;
            break;
        }else{
            mistake("unknown data.");
            return 0;
        }
    }
    return 1;
}
int getPData(char*file,PData*pdata){
    ptr=0;
    line=1;
    msg=NULL;
    fileName=file;
    if(!readFile(file)){
        return 0;
    }
    if(!getDat(pdata)){
        return 0;
    }
    free(code);
    return 1;
}
static void addBlock(char*text,int num){
    for(int i=0;i<num;i++){
        strcat(text,"    ");
    }
}
static void putDat(char*text,PData pdata,int depth){
    char temp[1024*8];
    char buf[1024];
    addBlock(text,depth);
    sprintf(temp,"(%s){\n",pdata.name);
    strcat(text,temp);
    for(int i=0;i<pdata.count;i++){
        putDat(text,pdata.dat[i],depth+1);
    }
    int len=strlen(pdata.text);
    if(len>0){
        addBlock(text,depth+1);
        char c;
        int i2=0;;
        for(int i=0;i<len;i++){
            c=pdata.text[i];
            if(c=='\n'){
                buf[i2++]='\\';
                buf[i2]='n';
            }else if(c=='\\'){
                buf[i2++]='\\';
                buf[i2]='\\';
            }else if(c=='\"'){
                buf[i2++]='\\';
                buf[i2]='\"';
            }else{
                buf[i2]=c;
            }
            i2++;
        }
        buf[i2]='\0';
        sprintf(temp,"\"%s\"\n",buf);
        strcat(text,temp);
    }
    addBlock(text,depth);
    strcat(text,"}\n");
}
int putPData(char*file,PData pdata){
    char text[1024*8];
    putDat(text,pdata,0);
    FILE*fp=fopen(file,"wt");
    if(fp==NULL){
        mistake("failed to write file.");
        return 0;
    }
    fwrite(text,1,strlen(text),fp);
    fclose(fp);
    return 1;
}
int searchPData(PData pdata,char*name){
    for(int i=0;i<pdata.count;i++){
        if(strcmp(name,pdata.dat[i].name)==0){
            return i;
        }
    }
    return -1;
}
void addPData(PData*pdata,PData child){
    pdata->count++;
    pdata->dat=(PData*)realloc(pdata->dat,pdata->count*sizeof(PData));
    pdata->dat[pdata->count-1]=child;
}