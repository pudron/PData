PData
======
![PDATA 图标](logo.png)
For English version,see [English](README_en.md)
## PData是数据结构编程语言，用于显式储存数据（如配置）
特点：
* 语言格式简单
* 库使用方便
* 可快速开发

PData基本格式：

    (数据名){
        子数据
        文本
    }

子数据可以无限嵌套，当出现多个文本时，PData会将它们合并。  
每个文件有且只有一个根数据，建议以文件名命名。  
库函数用法见[pdata.h](pdata.h)