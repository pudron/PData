PData 
=====
![PDATA icon](logo.png)
简体中文见[简体中文](README.md)
## PData is Data Structure   Programming Language used to store data or configure visibly.  
Feature:
* Simple language format 
* easy library usage
* fast development

Basic language format:  

    (data name){
        child data
        text
    }
The child data can be nested infinitely. When multiple texts appear, PData merges them.  
Each file has and only has one root data. It is recommended to name it with this file name.  
For the usage of library function,see [pdata.h](pdata.h)